<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/16/17
 * Time: 5:31 PM
 */

namespace Sandbox\Controller;


use Sandbox\SVG\SVGDocument;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RequestContext;

class HomePage {
  public function page() {
    $svg = new SVGDocument();
    $svg->generate();
    $content = $svg->render();
    return new Response($content);
  }
}