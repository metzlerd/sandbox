<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/18/17
 * Time: 6:19 PM
 */

namespace Sandbox\SVG;


abstract class SVGElement implements SVGElementInterface  {

  /** @var int */
  public $x=0;

  /** @var int */
  public $y=0;

}