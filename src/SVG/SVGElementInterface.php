<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/18/17
 * Time: 8:52 PM
 */

namespace Sandbox\SVG;


interface SVGElementInterface {

  public function render();

}