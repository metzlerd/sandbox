<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/18/17
 * Time: 6:58 PM
 */

namespace Sandbox\SVG;


class SVGDocument {
  /** @var SVGElement[] */
  protected $elements = [];

  public $width = 1000;

  public $height = 1000;

  public function render() {
    $o = $this;
    $height = $o->height;
    $width = $o->width;
    $content = '';
    foreach ($this->elements as $element) {
      $content .= $element->render();
    }
    $svg = "<svg viewBox='0 0 $height $width'>
      $content
    </svg>
    ";
    return $svg;
  }

  /**
   * Project rows onto the SVG document using a template element.
   */
  public function generate() {
    $labels = XLabels::fromLabels(["one", "two", 'three']);
    $this->elements = $labels;
    $this->distributeX($this->elements, 1000);
  }

  public function distributeX(array $elements, $x_width, $x_start=0) {
    $count = count($elements);
    if ($count>0) {
      $x_val = $x_start;
      $x_inc = round(($x_width - $x_start) / $count, 3);
      array_map(
         function($element) use (&$x_val, $x_inc) {
           $x_val = $x_val + $x_inc;
           $element->x = $x_val;
         },
        $elements
      );
    }
  }




}