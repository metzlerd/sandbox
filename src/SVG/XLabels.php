<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/18/17
 * Time: 6:58 PM
 */

namespace Sandbox\SVG;


class XLabels extends SVGElement {

  public $label;
  public $tick_height=100;

  public static function fromLabels(array $labels) {
    $elements = [];
    foreach ($labels as $label) {
      $o  = new static();
      $o->label = $label;
      $elements[] = $o;
    }
    return $elements;
  }

  public function render() {
    $x = $this->x;
    $y = $this->y;
    $y2 = $this->y + $this->tick_height;
    $label = $this->label;
    return
      "\n<g stroke='black' fill='black'><line x1='$x' y1='$y' x2='$x' y2='$y2'/><text x='$x' y='50' text-anchor='end'>$label</text></g>";
  }

}